package com.abnamro.apps.referenceandroid;


import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class HomePageTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void homePageTest()
    {
        try
        {
          //Test to validate all items are present on home page
          homeScreenTest();
          Log.d("MyTestApp", "Home Screen app test is successful");

          // Test to check sub-menu item under settings menu are displayed
          settingsMenuTest();
          Log.d("MyTestApp", "Settings Menu app test is successful");

            //Test to check Help screen is getting opened on clicking Help Menu Item
            helpMenuItemTest();
            Log.d("MyTestApp", "Help Menu Item test is successful");
        }
        catch(Error error)
        {
          Log.d("MyTestApp" + error, "Test failed");
        }
    }

    public void homeScreenTest() {
        ViewInteraction textView = onView(
                allOf(withText("MyTestApp"),isDisplayed()));
        textView.check(matches(withText("MyTestApp")));

        ViewInteraction textView2 = onView(
                allOf(withText("Welcome To TestApp"), isDisplayed()));
        textView2.check(matches(withText("Welcome To TestApp")));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.action_settings), withContentDescription("Settings"), isDisplayed()));
        textView3.check(matches(isDisplayed()));

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.action_help), withContentDescription("Help"), isDisplayed()));
        textView4.check(matches(isDisplayed()));

        ViewInteraction imageButton = onView(
                allOf(withId(R.id.fab), isDisplayed()));
        imageButton.check(matches(isDisplayed()));
    }

    public void settingsMenuTest() {
        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.action_settings), withContentDescription("Settings"), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.title), withText("GeneralSettings"), isDisplayed()));
        textView.check(matches(withText("GeneralSettings")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.title), withText("SecuritySettings"), isDisplayed()));
        textView2.check(matches(withText("SecuritySettings")));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.title), withText("NotificationSettings"), isDisplayed()));
        textView3.check(matches(withText("NotificationSettings")));
        textView3.perform(click());
    }

    public void helpMenuItemTest() {
        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.action_help), withContentDescription("Help"), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction textView = onView(
                allOf(withText("Help is coming Soon ...."), isDisplayed()));
        textView.check(matches(withText("Help is coming Soon ....")));

    }
}
