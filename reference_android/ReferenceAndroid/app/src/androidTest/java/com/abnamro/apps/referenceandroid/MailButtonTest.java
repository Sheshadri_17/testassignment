package com.abnamro.apps.referenceandroid;


import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MailButtonTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void onClickActivitytest() {
        try {
            //Test to validate all input boxes are shown on Email screen
            //User is able to input in the text boxes
            //User is able to come back to home screen
            mailFloatingButtonTest();
            Log.d("MyTestApp", "Mail test is successful");

        } catch (
                Error error) {
            Log.d("MyTestApp" + error, "Test failed");
        }
    }


    public void mailFloatingButtonTest() {
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab), isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction textView = onView(
                allOf(withText("To:"), isDisplayed()));
        textView.check(matches(withText("To:")));

        ViewInteraction textView2 = onView(
                allOf(withText("Subject:"), isDisplayed()));
        textView2.check(matches(withText("Subject:")));

        ViewInteraction textView3 = onView(
                allOf(withText("Message:"), isDisplayed()));
        textView3.check(matches(withText("Message:")));

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.edit_text_to), isDisplayed()));
        appCompatEditText.perform(replaceText("testmail@mail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.edit_text_subject), isDisplayed()));
        appCompatEditText2.perform(replaceText("Test Mail"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.edit_text_message), isDisplayed()));
        appCompatEditText3.perform(replaceText("This is a test mail"), closeSoftKeyboard());

        ViewInteraction button = onView(
                allOf(withId(R.id.send), isDisplayed()));
        button.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"), isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction textView4 = onView(
                allOf(withText("Welcome To TestApp"), isDisplayed()));
        textView4.check(matches(withText("Welcome To TestApp")));
    }
}