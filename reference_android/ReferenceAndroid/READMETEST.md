Development in App:
1) Added two menu items "Settings" and "Help"
   On clicking Settings three sub- menu item appears - "GeneralSettings", "SecuritySettings" , "NotificationSettngs"
   On clicking "Help" , help page is opened
  
2) Added background image on home screen

3) On clicking mail button, mail form is opened in which mail id, subject and message can be entered.
   On clicking back arrow user is back on the home screen
   
   
Automated test cases:
1) Check to validate title, text, menu items, sub-menu items on home screen.
   Validate clicking on help, help screen is opened

2) Validate on clicking mail button, mail form is opened.
   validate user is able to enter values in text boxes.
   Validate on clicking back arrow user is back on home screen.
   
  



